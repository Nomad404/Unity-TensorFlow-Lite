﻿using System;
using UnityEngine;
using TfLiteDelegate = System.IntPtr;

namespace TensorFlowLite
{
    public interface IGpuDelegate : IDisposable
    {
        TfLiteDelegate Delegate { get; }
    }

    public interface IBindableDelegate : IGpuDelegate
    {
        bool BindBufferToInputTensor(Interpreter interpreter, int index, ComputeBuffer buffer);

        bool BindBufferToOutputTensor(Interpreter interpreter, int index, ComputeBuffer buffer);
    }
}