﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TfLiteDelegate = System.IntPtr;
using TfLiteInterpreterOptions = System.IntPtr;

namespace TensorFlowLite
{
    public class InterpreterOptions : IDisposable
    {
        [UnmanagedFunctionPointer(CallingConvention.Cdecl, SetLastError = true)]
        private delegate void ErrorReporeterDelegate(IntPtr userData, string format, IntPtr argsPtrs);

        internal TfLiteInterpreterOptions nativePtr;
        private List<IGpuDelegate> _delegates;
        private int _threads;

        public int threads
        {
            get => _threads;
            set
            {
                _threads = value;
                TfLiteInterpreterOptionsSetNumThreads(nativePtr, value);
            }
        }

        private bool _useNNAPI;

        public bool useNNAPI
        {
            get => _useNNAPI;
            set
            {
                _useNNAPI = value;
#if UNITY_ANDROID && !UNITY_EDITOR
InterpreterExperimental.TfLiteInterpreterOptionsSetUseNNAPI(nativePtr, value);
#endif // UNITY_ANDROID && !UNITY_EDITOR
            }
        }

        public InterpreterOptions()
        {
            nativePtr = TfLiteInterpreterOptionsCreate();
            _delegates = new List<IGpuDelegate>();

            ErrorReporter.ConfigureReporter(nativePtr);
        }

        public void Dispose()
        {
            if (nativePtr != IntPtr.Zero)
            {
                TfLiteInterpreterOptionsDelete(nativePtr);
            }

            foreach (var gpuDelegate in _delegates)
            {
                gpuDelegate.Dispose();
            }

            _delegates.Clear();
        }

        public void AddGpuDelegate(IGpuDelegate gpuDelegate)
        {
            if (gpuDelegate == null) return;
            TfLiteInterpreterOptionsAddDelegate(nativePtr, gpuDelegate.Delegate);
            _delegates.Add(gpuDelegate);
        }

        public void AddGpuDelegate()
        {
            AddGpuDelegate(CreateGpuDelegate());
        }

        private static IGpuDelegate CreateGpuDelegate()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            return new GpuDelegateV2();
#elif UNITY_IOS || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            return new MetalDelegate(new MetalDelegate.Options(){
                allowPrecisionLoss = false,
                waitType = MetalDelegate.WaitType.Passive,
                enableQuantization = true,
            });
#endif
            UnityEngine.Debug.LogWarning("GPU Delegate is not supported on this platform");
            return null;
        }

        #region External

        private const string TensorFlowLibrary = Interpreter.TensorFlowLibrary;

        [DllImport(TensorFlowLibrary)]
        private static extern unsafe TfLiteInterpreterOptions TfLiteInterpreterOptionsCreate();

        [DllImport(TensorFlowLibrary)]
        private static extern unsafe void TfLiteInterpreterOptionsDelete(TfLiteInterpreterOptions options);

        [DllImport(TensorFlowLibrary)]
        private static extern unsafe void TfLiteInterpreterOptionsSetNumThreads(TfLiteInterpreterOptions options,
            int num_threads);

        [DllImport(TensorFlowLibrary)]
        private static extern unsafe void TfLiteInterpreterOptionsAddDelegate(TfLiteInterpreterOptions options,
            TfLiteDelegate _delegate);

        #endregion
    }
}